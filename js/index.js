$(document).ready(function () {
    landingPageAdjusment();
    $("#logoImg").click(function(){
        location.reload(true);
    });
    $('.overlay').css({"width" : "0 !important"});
    $('#gif_section').show();
    $('#carousel_section').hide();
    $('#menuSec').hide();
    $('#who_we_are_section').hide();
    $('#our_beliefs_section').hide();
    $('#design_philosophy').hide();
    $('#our_team_section').hide();
    $('#what_we_do').hide();
    $('#contact_us_section').hide();

});  

function landingPageAdjusment() {
    height = $(window).height();
    width = $(window).width();
    $('#menuSec').hide();
    carousel_caption_height = height / 2;
    $('#gif_img').css('height', height);
    $('#gif_img').css('width', width);
    $('.carousel-item img').css('height', height - 70);
    $('.carousel-item img').css('width', width);
    $('.carousel-caption').css('height', height);
    $('.who_we_are_section').css('height', height - 70);
    $('.our_beliefs_section').css('height', height);
    $('.design_philosophy').css('height', height);
    $('.design_philosophy').css('width', width);
    $('.what_we_do').css('height', height);
    $('.what_we_do').css('width', width);
}
$(function() {
    $(".downArrow").delay(4000).fadeIn();
});

function openNav() {
    var height = $(window).height();
    var width = $(window).width();
    if( width <= 720){
        document.getElementById("myNav").style.width = "80%";
        document.getElementById("myNav").style.overflow = "hidden";
    } else {
        document.getElementById("myNav").style.width = "25%";
        document.getElementById("myNav").style.overflow = "hidden";
    }
    document.getElementsByClassName("carousel-indicators").style.display = "none";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("myNav").style.overflow = "hidden";

    document.getElementsByClassName("carousel-indicators").style.display = "block";
}

var carouselContainer = $('.carousel');
var slideInterval = 900000;
function toggleH() {
    $('.toggleHeading').hide()
    var caption = carouselContainer.find('.active').find('.toggleHeading').addClass('animated fadeInRight').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
        function () {
            $(this).removeClass('animated fadeInRight')
        });
    caption.slideToggle();
}
carouselContainer.carousel({ interval: slideInterval, cycle: true, pause: "hover" }).on('slide.bs.carousel slid.bs.carousel', toggleH).trigger('slide.bs.carousel')
$('#show_carousel_section').on('click', function () {
    $('#show_carousel_section').hide();
    $('#gif_section').hide();
    $('#carousel_section').show();
    $('#menuSec').show();
    $('#who_we_are_section').show();
    $('#our_beliefs_section').show();
    $('#design_philosophy').show();
    $('#our_team_section').show();
    $('#what_we_do').show();
    $('#contact_us_section').show();
});


//Nav bar sticky//

var header = document.getElementById("menuSec");
var sticky = header.offsetTop;
var ypos = window.pageYOffset;
function myFunction() {
    if (ypos >= sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}
  
function scrollFunction(){
    var elmnt = document.getElementById("who_we_are_section");
    elmnt.scrollIntoView();
}

